call plug#begin('~/.config/nvim/plugged')

" text and lightline theme
Plug 'rakr/vim-one'
Plug 'itchyny/lightline.vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'sheerun/vim-polyglot'
Plug 'preservim/nerdtree'

call plug#end()

if (empty($TMUX)) " Color support for terminal colors
	if (has("nvim"))
		let $NVIM_TUI_ENABLE_TRUE_COLOR=1
	endif
	if (has("termguicolors"))
		set termguicolors
	endif
endif

set noshowmode " Disable showing vimmodes
set background=dark " Alternate to light
colorscheme one
let g:lightline = { 'colorscheme':'one', }

" On insert mode set relative line numbers on 
" otherwise, absolute line numbers
:set number relativenumber
:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

" Open nerdtree by default
" autocmd vimenter * NERDTree

" Map keys for tabbed workspaces
map <C-t><up> :tabr<cr>
map <C-t><down> :tabl<cr>
map <C-t><left> :tabp<cr>
map <C-t><right> :tabn<cr>

" Map keys for split workspaces


set list
set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+

set tabstop=2
set shiftwidth=2
" AutoLint rust on every save
let g:rustfmt_autosave = 1
" let g:coc_global_extensions=[ 'coc-omnisharp', ... ]
