#!/bin/bash

# This bash script creates symlinks of these configs
# Towards the specified directory
 
# Typically where the configs are usually stored
# Change if necessary
CFG_DIR=~/.config

# Create symlinks
# Safely modify the cfgs in the CWD
ln -s $PWD/i3 $CFG_DIR/
ln -s $PWD/nvim $CFG_DIR/
ln -s $PWD/alacritty $CFG_DIR/


