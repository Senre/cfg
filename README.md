# cfg
This repo is for storing my *dotfiles*.

### Configs
This repo contains the configuration files for the following applications.

* [i3-gaps]() - A tiling window manager. (For the icons to work, you should install [ttf-font-awesome](https://www.archlinux.org/packages/community/any/ttf-font-awesome/))
* [alacritty]() - Only has atom one dark theme for now.
* [neovim]() - Only has atom one dark theme for now

### Installation

Clone to your home directory. It is recommended to fork this repo so you can configure it for yourself.
```sh
$ cd ~
$ git clone https://gitlab.com/Senre/cfg.git # Your repo address
$ mv cfg .cfg # rename to .cfg optionally
```


Create symlinks to `~/.config/`

```sh
$ chmod +x install.sh
$ sh install.sh
```

Then you can work on your configs in your fork without actually going to your *.config/* folder.


### Todos

 - Add more themes and functionality
 - Improve installation scripts
